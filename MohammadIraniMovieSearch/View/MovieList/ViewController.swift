//
//  ViewController.swift
//  MohammadIraniMovieSearch
//
//  Created by Macbook pro on 26/01/19.
//  Copyright © 2019 MohammadIrani. All rights reserved.
//

import UIKit
import Alamofire

protocol MovieListView: NSObjectProtocol {
    func finishLoading(movies: [Movie])
}


class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    
    @IBOutlet weak var searchBarOutlet: UISearchBar!
    private let presenter = ViewControllerPresentor()
    
    //////collection sizes
    var estimateWidth = 160.0
    var cellMarginSize = 4.0
    
    var tableData: [Movie]?
    var data: [Movie]?
    ///////Array List
    //var data:Array< Movie > = Array < Movie >()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.setView(view: self as! MovieListView)
        
        self.searchBarOutlet.delegate = self
        
        self.collectionViewOutlet.delegate = self
        self.collectionViewOutlet.dataSource = self
        //self.collectionView.register(UINib(nibName: "MovieListCell", bundle: nil), forCellReuseIdentifier: "MovieListCell")
        
        self.collectionViewOutlet.register(UINib(nibName: "MovieListCell2", bundle: nil), forCellWithReuseIdentifier: "MovieListCell2")
        
        //////////Clear the background of search Bar
        let image = UIImage()
        searchBarOutlet.setBackgroundImage(image, for: .any, barMetrics: .default)
        searchBarOutlet.scopeBarBackgroundImage = image
        //////////Clear the background of search Bar

        ////////Search Bar Text Font Size
        searchBarOutlet.change(textFont: UIFont(name:"FontAwesome",size:10))

        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    ///////SerachBar
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for subView in searchBarOutlet.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    var bounds: CGRect
                    bounds = textField.frame
                    bounds.size.height = 30 //(set height whatever you want)
                    textField.bounds = bounds
                    textField.borderStyle = UITextField.BorderStyle.roundedRect
                    //                    textField.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
                    textField.backgroundColor = UIColor.white
                    //                    textField.font = UIFont.systemFontOfSize(20)
                    
                    
                    textField.leftViewMode = UITextField.ViewMode.never
                }
            }
        }
        
    }
    
    @IBAction func FilterButtonTapped(_ sender: Any) {
    
        let sortedArray = tableData?.sorted {
            $0.vote_average > $1.vote_average
        }
        
        self.data = sortedArray!
        
        collectionViewOutlet.reloadData()
        
        // the alert view
        let alert = UIAlertController(title: "", message: "Movie has been sorted as per High to Low Rating", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.presenter.loadGenreAndMovies()
        
        // TODO: add loading view
        // TODO: error handler and alerts / retry
    }
    
  
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //    self.performSegue(withIdentifier: "movieDetailSegue", sender: self)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        let movie = self.data?[indexPath.row]
        
        nextViewController.setMovie(movie: movie!)

        
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
     func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
                if self.presenter.canLoadMore(indexPath: indexPath) {
                    self.presenter.loadNextPage()
                }

        
    }

    func setupGridView()
    {
        let flow = collectionViewOutlet?.collectionViewLayout as! UICollectionViewFlowLayout
        
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupGridView()
        DispatchQueue.main.async {
            self.collectionViewOutlet.reloadData()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

        
        if let sender = sender as? UICollectionViewCell {
            let indexPath = self.collectionViewOutlet.indexPath(for: sender)
            // Whatever else you want to do
            let movie = self.data?[indexPath?.row ?? 0]
            if let destination = segue.destination as? MovieDetailView {
                destination.setMovie(movie: movie!)
                            }
        }
        
       

    }
    

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.data?.count ?? 0
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MovieListCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieListCell2", for: indexPath) as! MovieListCell2
        
                if let movie = data?[indexPath.row] {
                    cell.titleLabelNew.text = movie.title
                 //   cell.dateLabel.text = movie.release_date
                 //   cell.genreLabel.text = movie.genresString
                    let userRating = "RATING: "
                    cell.ratingLabel.text = userRating + String(movie.vote_average)
        
                    cell.coverImageView.image = nil
                    if let imageURL = MovieService.smallCoverUrl(movie: movie) {
                        cell.coverImageView.load(url: imageURL)
                    }
                }
        
                return cell
        
    }
    
}

extension ViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        //https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher

//        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { // Change `2.0` to the desired number of seconds.
//            // Code you want to be delayed
//
//            if let url = URL(string: "https://api.themoviedb.org/3/search/movie?api_key=39310ddf12dc3725a56e89e627af5844&query=\(searchText)")
//            {
//                var urlRequest = URLRequest(url: url)
//                let manager = Alamofire.SessionManager.default
//                manager.session.configuration.timeoutIntervalForRequest = 120
//
//                manager.request(urlRequest)
//                    .responseJSON { response in
//                }
//            }
//        }
        
        
        guard !searchText.isEmpty else
        {
            data = tableData
            collectionViewOutlet.reloadData()
            
            return
        }
        
        data = tableData?.filter({ (da) -> Bool in
            
            da.title.lowercased().contains(searchText.lowercased())
        })
        collectionViewOutlet.reloadData()

        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        
        searchBar.resignFirstResponder()
    }
    
}



extension ViewController: MovieListView {
    
    func finishLoading(movies: [Movie]) {
        self.tableData = movies
        self.data = movies
        self.collectionViewOutlet.reloadData()
        // TODO: add callback for error!!!
    }
    
    
}

/////UICollectionViewDelegateFlowLayout

extension ViewController: UICollectionViewDelegateFlowLayout
{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let width = self.calculateWidth()
//        return CGSize(width: width, height: 200)
//    }
//
//    func calculateWidth() -> CGFloat
//    {
//        let estimateWidth2 = CGFloat(estimateWidth)
//        let cellCount = floor(CGFloat(self.view.frame.size.width / estimateWidth2))
//
//        let margin = CGFloat(cellMarginSize * 1)
//        let width =  (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
//
//        return width
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let numberOfColumns: CGFloat = 3
        let width = collectionView.frame.size.width
        
        let xInsets: CGFloat = 2
        let cellSpacing: CGFloat = 2
        
        
        return CGSize(width: (width / numberOfColumns) - (xInsets + cellSpacing), height: (250) - (xInsets + cellSpacing))
    }
    
}


extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    } }

