//
//  DetailViewControllerPresentor.swift
//  MohammadIraniMovieSearch
//
//  Created by Macbook pro on 26/01/19.
//  Copyright © 2019 MohammadIrani. All rights reserved.
//

import Foundation



class DetailViewControllerPresentor {
    
    weak private var view: MovieDetailView?
    
    var movie: Movie!
    
    func setView(view: MovieDetailView) {
        self.view = view
    }
    
    func showMovie() {
        self.view?.updateView(title: movie.title, date: movie.release_date, genre: movie.genresString, overview: movie.overview, vote_average: movie.vote_average)
        
        if let imageURL = MovieService.bigCoverUrl(movie: movie) {
            self.view?.loadImage(url: imageURL)
        }
    }
    
}

