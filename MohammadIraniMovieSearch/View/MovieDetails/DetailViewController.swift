//
//  DetailViewController.swift
//  MohammadIraniMovieSearch
//
//  Created by Macbook pro on 26/01/19.
//  Copyright © 2019 MohammadIrani. All rights reserved.
//

import UIKit

protocol MovieDetailView: NSObjectProtocol {
    func setMovie(movie: Movie)
    func updateView(title: String, date: String, genre: String, overview: String, vote_average: Double)
    func loadImage(url: URL)
}

class DetailViewController: UIViewController {
    
    private let presenter = DetailViewControllerPresentor()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!

    @IBOutlet weak var ratingLabel: UILabel!
    
    var userRating = "USER RATING : "
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.presenter.setView(view: self as! MovieDetailView)
        self.presenter.showMovie()

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension DetailViewController: MovieDetailView {
    func updateView(title: String, date: String, genre: String, overview: String, vote_average: Double) {
        self.title = title
        self.titleLabel.text = title
        self.dateLabel.text = date
        self.genreLabel.text = genre
        self.overviewLabel.text = overview
        
        let strRating = userRating + String(vote_average)
        self.ratingLabel.text = strRating
    }
    
    
    func setMovie(movie: Movie) {
        self.presenter.movie = movie
    }
    
//    func updateView(title: String, date: String, genre: String, overview: String) {
//
//    }
    
    func loadImage(url: URL) {
        self.coverImageView.load(url: url)
    }
    
}

