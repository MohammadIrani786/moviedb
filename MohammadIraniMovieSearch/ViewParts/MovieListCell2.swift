//
//  MovieListCell2.swift
//  MohammadIraniMovieSearch
//
//  Created by Macbook pro on 26/01/19.
//  Copyright © 2019 MohammadIrani. All rights reserved.
//

import UIKit

class MovieListCell2: UICollectionViewCell {
    
    
    @IBOutlet weak var titleLabelNew: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
