//
//  Genre.swift
//  MohammadIraniMovieSearch
//
//  Created by Macbook pro on 26/01/19.
//  Copyright © 2019 MohammadIrani. All rights reserved.
//

import Foundation

struct Genre: Decodable {
    var id: Int
    var name: String
}
